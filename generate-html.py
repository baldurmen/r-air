#!/usr/bin/python3

"""
This script generates the final HTML files from jinja2 templates and the latest
data dump
"""

import argparse
import pathlib
import pickle
import sys

from datetime import datetime

import jinja2


sensors_dict = {'pm1': {'name': 'PM<sub>1</sub>',
                        'var': 'pm1',
                        'color': '#B40F20',
                        'unit': ''},
                'pm2_5': {'name': 'PM<sub>2.5</sub>',
                          'var': 'pm2_5',
                          'color': '#0A9F9D',
                          'unit': ''},
                'pm10': {'name': 'PM<sub>10</sub>',
                         'var': 'pm10',
                         'color': '#F98400',
                         'unit': ''},
                'humidity': {'name': 'Humidité',
                             'var': 'humidity',
                             'color': '#9986A5',
                             'unit': '%'},
                'temperature': {'name': 'Température',
                                'var': 'temperature',
                                'color': '#FD6467',
                                'unit': '°C'},
                'pressure': {'name': 'Pression',
                             'var': 'pressure',
                             'color': '#6C8645',
                             'unit': 'hPa'},
                }


def parse_args(sys_args):  # pylint: disable=W0613
    """CLI argument comprehension"""
    example_text = '''example:
        generate-html -t "/path/to/template/directory" -o "/path/to/output/directory" -p "/path/to/the/pickled/data"'''
    parser = argparse.ArgumentParser(prog='revolvair-to-sqlite',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=__doc__,
                                     epilog=example_text)
    parser.add_argument('-t', '--template-dir', type=pathlib.Path, required=True,
                        help='path to the template directory')
    parser.add_argument('-o', '--output-dir', type=pathlib.Path, required=True,
                        help='path to the HTML output directory')
    parser.add_argument('-p', '--pickle', type=pathlib.Path, required=True,
                        help='path to the pickled data')
    return parser.parse_args()


def process_pickle(pickled_data):
    """Process the pickled data for rendering"""
    with open(pickled_data, 'rb') as _pickle:
        pickled = pickle.load(_pickle)
    for key in pickled:
        if key == 'pressure':
            pickled[key] = round(float(pickled[key])/100)
        elif key != 'date':
            pickled[key] = round(float(pickled[key]))
    jinja_data = pickled
    return jinja_data


def write_file(data, output):
    """Write rendered file to disk"""
    with open(output, 'w', encoding='utf-8') as _file:
        _file.write(data)


def render_html(jinja_data, template_dir, output_dir):
    """Render the final HTML files based on jinja templates"""
    templateLoader = jinja2.FileSystemLoader(searchpath=template_dir)
    templateEnv = jinja2.Environment(loader=templateLoader)
    t_index = templateEnv.get_template('index.html.j2')
    r_index = t_index.render(jinja_data)
    write_file(r_index, output_dir / 'index.html')
    t_graph = templateEnv.get_template('graph_page.html.j2')
    for key, sensor in sensors_dict.items():
        r_graph = t_graph.render(name=sensor['name'],
                                 var=sensor['var'],
                                 value=jinja_data[key],
                                 color=sensor['color'],
                                 unit=sensor['unit'],
                                 yearmon=datetime.now().strftime("%Y-%m"))
        write_file(r_graph, output_dir / f"{sensor['var']}.html")


def main():
    """Main function."""
    args = parse_args(sys.argv[1:])
    jinja_data = process_pickle(args.pickle)
    render_html(jinja_data, args.template_dir, args.output_dir)


if __name__ == "__main__":
    main()
