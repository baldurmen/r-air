#!/usr/bin/python3

"""
This script fetches the JSON data from a revolvair.org device and stores it as
a time series in a sqlite database
"""

import argparse
import json
import pathlib
import pickle
import sqlite3
import sys
import time

from datetime import datetime

import requests


def parse_args(sys_args):  # pylint: disable=W0613
    """CLI argument comprehension"""
    example_text = '''example:
        revolvair-to-sqlite -d "/path/to/db.sqlite3" -u "http://1.1.1.1/data.json"'''
    parser = argparse.ArgumentParser(prog='revolvair-to-sqlite',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=__doc__,
                                     epilog=example_text)
    parser.add_argument('-d', '--db', type=pathlib.Path, required=True,
                        help='path to the sqlite database')
    parser.add_argument('-o', '--output', type=pathlib.Path,
                        help='optional pickle object including the latest data')
    parser.add_argument('-j', '--joutput', type=pathlib.Path,
                        help='optional json file including the latest data')
    parser.add_argument('-u', '--url', type=str, required=True,
                        help='URL for the data.json file')
    return parser.parse_args()


def fetch_data(url):
    """Fetch JSON data from Revolvair device"""
    json_data = requests.get(url, timeout=5)
    return json_data


def json_to_dict(json_data):
    """Process JSON data from Revolvair device and output a dict"""
    processed_data = {}
    processed_data['date'] = time.mktime(datetime.now().timetuple())
    json_obj = json.loads(json_data.text)
    for value in json_obj['sensordatavalues']:
        if value['value_type'] == 'PMS_P0':
            processed_data['pm1'] = value['value']
        elif value['value_type'] == 'PMS_P2':
            processed_data['pm2_5'] = value['value']
        elif value['value_type'] == 'PMS_P1':
            processed_data['pm10'] = value['value']
        elif value['value_type'] == 'BME280_temperature':
            processed_data['temperature'] = value['value']
        elif value['value_type'] == 'BME280_pressure':
            processed_data['pressure'] = value['value']
        elif value['value_type'] == 'BME280_humidity':
            processed_data['humidity'] = value['value']
    return processed_data


def sqlite_create_table(cursor):
    """Create sqlite table if it does not exist"""
    create_table = """
    CREATE TABLE IF NOT EXISTS revolvair(
        date REAL,
        pm1 REAL,
        pm2_5 REAL,
        pm10 REAL,
        temperature REAL,
        pressure REAL,
        humidity REAL)"""
    cursor.execute(create_table)


def sqlite_import(db, processed_data):
    """Import data into the sqlite database"""
    con = sqlite3.connect(db)
    cur = con.cursor()
    sqlite_create_table(cur)
    cur.execute("""
    INSERT INTO revolvair
        (date, pm1, pm2_5, pm10, temperature, pressure, humidity)
    VALUES
        (:date, :pm1, :pm2_5, :pm10, :temperature, :pressure, :humidity)""",
        processed_data)
    con.commit()
    con.close()


def pickle_export(processed_data, output):
    """Export data to a pickle, to be used in other scripts"""
    with open(output, 'wb') as pout:
        pickle.dump(processed_data, pout)


def json_export(json_data, output):
    """Export raw JSON data as-is, to be used in other scripts"""
    with open(output, 'w') as pout:
        pout.write(json_data.text)


def main():
    """Main function."""
    args = parse_args(sys.argv[1:])
    json_data = fetch_data(args.url)
    processed_data = json_to_dict(json_data)
    sqlite_import(args.db, processed_data)
    pickle_export(processed_data, args.output)
    json_export(json_data, args.joutput)


if __name__ == "__main__":
    main()
