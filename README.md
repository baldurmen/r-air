This repository is a collection of tools to collect and graph data from a
[RevolvAir](https://revolvair.org/) station, using R and Python. Tools to create
a website from these graphs are also available in this repository.

An example of the final website produced can be found at:
<https://air.veronneau.org>

# Dependencies

To run `revolvair-graphs.R`, you will need the following dependencies:

```
$ apt install r-base r-cran-ggplot2 r-cran-optparse r-cran-plyr r-cran-rsqlite r-cran-lubridate
```

To run `revolvair-to-sqlite.py` and `generate-html.py`, the following dependencies are needed:

```
$ apt install python3-requests python3-jinja2
```

# Instructions

These tools can be used in three discrete steps:

## 1. Collect the data from the RevolvAir device

This step uses the `revolvair-to-sqlite.py` script. It can be called as such:

```
$ ./revolvair-to-sqlite.py --db "/path/to/db.sqlite3" --url "http://1.1.1.1/data.json"
```

The output should be an SQLite database file ready to be used during the second
step.

This script will need to be ran regularly (preferably each 150 seconds) to
gather data from the RevolvAir device and build a time series. This can be done
either using a cronjob, or (preferably) a systemd timer.

Here is an example of a cronjob:

```
2-57/5 * * * * MY_USER sleep 30; /path/to/revolvair-to-sqlite.py --db "/path/to/db.sqlite3" --url "http://1.1.1.1/data.json"
*/5 * * * * MY_USER /path/to/revolvair-to-sqlite.py --db "/path/to/db.sqlite3" --url "http://1.1.1.1/data.json"
```

And an example of a systemd timer:

```
[Timer]
OnCalendar=*-*-* *:0/2:30
OnActiveSec=0
Unit=revolvair-to-sqlite.service
AccuracySec=1us
```

Note that if you want to build a website (step 3), you will need to run all the
`revolvair-to-sqlite.py` commands listed above with the `--output` parameter:

```
$ ./revolvair-to-sqlite.py --db "/path/to/db.sqlite3" --url "http://1.1.1.1/data.json" --output '/path/to/latest.pickle'
```

## 2. Parse and graph the data

This step uses the `revolvair-graphs.R` script. I can be called as such:

```
$ ./revolvair-graphs.R --db '/path/to/db.sqlite3' --outdir '/path/to/outdir'
```

As for the first step, this script should be called regularly, to update the
graphs. A longer iteration is recommended, since running this script takes more
computer ressources to run.

Here is an example of a cronjob you can run:

```
*/5 * * * * MY_USER sleep 5; /usr/local/bin/revolvair-graphs.R --db '/path/to/db.sqlite3' --outdir '/path/to/outdir'
```

## 3. Build the website

This steps uses the `generate-html.py` script, which can called this way:

```
$ ./generate-html.py --template-dir '/path/to/template/directory' --output '/path/to/output/directory' --pickle '/path/to/the/pickle/data'
```

The result should be a static website ready to be put online. If you use this
script, please consider changing the template remove my name and other personal
data from it :).
